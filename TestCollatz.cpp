// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <sstream>  // istringtstream, ostringstream
#include <tuple>    // make_tuple, tuple
#include <utility>  // make_pair, pair

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// ----
// read
// ----

TEST(CollatzFixture, read) {
    ASSERT_EQ(collatz_read("1 10\n"), make_pair(1, 10));
}

// ----
// eval
// ----

TEST(CollatzFixture, eval0) {
    ASSERT_EQ(collatz_eval(make_pair(1, 10)), make_tuple(1, 10, 20));
}

TEST(CollatzFixture, eval1) {
    ASSERT_EQ(collatz_eval(make_pair(100, 200)), make_tuple(100, 200, 125));
}

TEST(CollatzFixture, eval2) {
    ASSERT_EQ(collatz_eval(make_pair(201, 210)), make_tuple(201, 210, 89));
}

TEST(CollatzFixture, eval3) {
    ASSERT_EQ(collatz_eval(make_pair(900, 1000)), make_tuple(900, 1000, 174));
}

TEST(CollatzFixture, eval4) {
    ASSERT_EQ(collatz_eval(make_pair(500, 5000)), make_tuple(500, 5000, 238));
}

TEST(CollatzFixture, eval5) {
    ASSERT_EQ(collatz_eval(make_pair(500, 5002)), make_tuple(500, 5002, 238));
}

TEST(CollatzFixture, eval6) {
    ASSERT_EQ(collatz_eval(make_pair(750, 5050)), make_tuple(750, 5050, 238));
}

TEST(CollatzFixture, eval7) {
    ASSERT_EQ(collatz_eval(make_pair(5000, 500)), make_tuple(5000, 500, 238));
}

TEST(CollatzFixture, eval8) {
    ASSERT_EQ(collatz_eval(make_pair(3, 9)), make_tuple(3, 9, 20));
}

TEST(CollatzFixture, eval9) {
    ASSERT_EQ(collatz_eval(make_pair(9, 3)), make_tuple(9, 3, 20));
}

TEST(CollatzFixture, eval10) {
    ASSERT_EQ(collatz_eval(make_pair(999999, 999999)), make_tuple(999999, 999999, 259));
}

TEST(CollatzFixture, eval11) {
    ASSERT_EQ(collatz_eval(make_pair(999998, 999999)), make_tuple(999998, 999999, 259));
}

TEST(CollatzFixture, eval12) {
    ASSERT_EQ(collatz_eval(make_pair(1, 999999)), make_tuple(1, 999999, 525));
}

TEST(CollatzFixture, eval13) {
    ASSERT_NE(collatz_eval(make_pair(1, 999999)), make_tuple(1, 999999, 1));
}

TEST(CollatzFixture, eval14) {
    ASSERT_NE(collatz_eval(make_pair(1, 999999)), make_tuple(1, 999999, 524));
}

TEST(CollatzFixture, eval15) {
    ASSERT_NE(collatz_eval(make_pair(65, 847)), make_tuple(65, 847, 354));
}

TEST(CollatzFixture, eval16) {
    ASSERT_NE(collatz_eval(make_pair(9, 3)), make_tuple(3, 9, 20));
}

TEST(CollatzFixture, eval17) {
    ASSERT_NE(collatz_eval(make_pair(6884, 46884)), make_tuple(6884, 46884, 684));
}

TEST(CollatzFixture, eval18) {
    ASSERT_NE(collatz_eval(make_pair(5, 5)), make_tuple(5, 5, 5));
}

TEST(CollatzFixture, eval19) {
    ASSERT_NE(collatz_eval(make_pair(4, 4)), make_tuple(4, 4, 24));
}

TEST(CollatzFixture, eval20) {
    ASSERT_NE(collatz_eval(make_pair(100650, 83907)), make_tuple(100650, 83907, 174));
}

TEST(CollatzFixture, eval21) {
    ASSERT_NE(collatz_eval(make_pair(730, 80934)), make_tuple(730, 80934, 87));
}

TEST(CollatzFixture, eval22) {
    ASSERT_NE(collatz_eval(make_pair(4, 45)), make_tuple(4, 45, 345));
}

TEST(CollatzFixture, eval23) {
    ASSERT_EQ(collatz_eval(make_pair(1, 2)), make_tuple(1, 2, 2));
}

// -----
// print
// -----

TEST(CollatzFixture, print) {
    ostringstream sout;
    collatz_print(sout, make_tuple(1, 10, 20));
    ASSERT_EQ(sout.str(), "1 10 20\n");
}

// -----
// solve
// -----

TEST(CollatzFixture, solve) {
    istringstream sin("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n");
}
