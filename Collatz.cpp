// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair
#include <cmath>    // to use floor
#include <unordered_map>      // to create a cache

#include "Collatz.hpp"

using namespace std;

// global cache
std::unordered_map<long, int> cache;

// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (const string& s)
{
    istringstream sin(s);
    int i, j;
    sin >> i >> j;
    return make_pair(i, j);
}

// ------------
// cycle_length
// ------------
long cycle_length (long n)
{
    assert (n > 0);
    assert (n < 1000000);

    // calculates the cycle length
    long c = 1;
    while (n > 1)
    {
        // if already in cache, stop
        if (cache.find(n) != cache.end())
        {
            c += cache[n] - 1;
            break;
        }

        // if even, divide by 2
        if (n % 2 == 0)
        {
            n = n >> 1;
            c += 1;
        }
        // if odd, multiple by 3 and add 1
        else
        {
            n = n + (n >> 1) + 1;
            c += 2;
        }
    }

    assert (c > 0);
    return c;

}

// ------------
// collatz_eval
// ------------

tuple<int, int, int> collatz_eval (const pair<int, int>& p)
{
    int i, j;
    tie(i, j) = p;

    // HackerRank requirements check
    assert (i < 1000000 && j < 1000000);
    assert (i > 0 && j > 0);

    // set lower and upper bounds
    long lower;
    long upper;
    if (i <= j)
    {
        lower = i;
        upper = j;
    }
    else
    {
        lower = j;
        upper = i;
    }

    // compute the max cycle length
    if (i < floor(j/2))
    {
        lower = floor(j/2);
        upper = j;
    }

    // look for the max cycle length
    long m = 0;
    for (long n = lower; n <= upper; n++)
    {
        long c = 0;

        // check to see if it's in the cache
        if (cache.find(n) != cache.end())
        {
            c = cache[n];
        }
        // if not already in the cache, add it
        else
        {
            c = cycle_length(n);
            cache[n] = c;
        }

        // checks to see if it's the max
        if (c > m)
        {
            m = c;
        }
    }

    return make_tuple(i, j, m);
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& sout, const tuple<int, int, int>& t)
{
    int i, j, v;
    tie(i, j, v) = t;
    sout << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& sin, ostream& sout)
{
    string s;
    while (getline(sin, s))
        collatz_print(sout, collatz_eval(collatz_read(s)));
}
